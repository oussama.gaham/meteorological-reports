public class WeatherStation {
	public static void main(String[] args) {
		ConcreteObservable weatherData = new ConcreteObservable();

		ConcreteObserver currentConditions = new ConcreteObserver(weatherData);

		weatherData.setMeasurement(80, 65, 30.4f);
		weatherData.setMeasurement(90, 60, 32.2f);
		weatherData.setMeasurement(85, 67, 29.5f);
	}
}

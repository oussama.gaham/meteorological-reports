import lombok.Data;

import java.util.Observable;

@Data
public class ConcreteObservable extends Observable {
	private float temperature;
	private float humidity;
	private float pressure;

	public ConcreteObservable() {
	}

	public void measurementChanged(){
		notifyObservers();
	}

	public void setMeasurement(float temperature, float humidity, float pressure){
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementChanged();
	}
}

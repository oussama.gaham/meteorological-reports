import java.util.Observable;
import java.util.Observer;

public class ConcreteObserver implements Observer {
	private float temperature;
	private float humidity;

	private ConcreteObservable weatherData;

	public ConcreteObserver(ConcreteObservable weatherData) {
		this.weatherData = weatherData;
		weatherData.addObserver(this);
	}

	@Override
	public void update(Observable observable, Object o) {
		ConcreteObservable wd = (ConcreteObservable) o;
		this.temperature = wd.getTemperature();
		this.humidity = wd.getHumidity();
		display();
	}

	public void display(){
		System.out.printf("Current conditions: %fF degrees and %f% humidity", temperature, humidity);
	}
}
